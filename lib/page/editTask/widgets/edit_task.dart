import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:time_range_picker/time_range_picker.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/helpers/application_helper.dart';
import 'package:todo_app/page/editTask/bloc/edittask_bloc.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:todo_app/page/home/models/type_task_entity.dart';

class EditTask extends StatefulWidget {
  final Task task;
  EditTask(this.task);

  @override
  _EditTaskState createState() => _EditTaskState();
}

class _EditTaskState extends State<EditTask> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => EditTaskBloc(),
      child: EditTaskView(widget.task),
    );
  }
}

class EditTaskView extends StatefulWidget {
  final Task task;
  EditTaskView(this.task);

  @override
  _EditTaskViewState createState() => _EditTaskViewState();
}

DateTime? selectedDate;
String timeStart = "";
String timeEnd = "";
TextEditingController _controllerContent = TextEditingController();
int indexTask = -1;
var items = ['Completed', 'InCompleted'];
String dropdownvalue = "InCompleted";

class _EditTaskViewState extends State<EditTaskView> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<EditTaskBloc, EditTaskState>(
      listener: (context, state) {
        if (state.status == EditTaskStatus.Success) {
          ApplicationHelper.shared.hideDialogProgress(context);
          _controllerContent.text = "";
          Navigator.pop(context);
        } else if (state.status == EditTaskStatus.Processing) {
          ApplicationHelper.shared.showDialogProgress(context);
        } else if (state.status == EditTaskStatus.Failed) {
          ApplicationHelper.shared.hideDialogProgress(context);
        }
      },
      builder: (context, state) {
        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              top: MediaQuery.of(context).size.height * 0.2,
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.elliptical(150, 30)),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        'Edit task',
                        style: TextStyle(
                            color: AppColors.color404040,
                            fontSize: 22,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 40,
                        child: TextFormField(
                          style: TextStyle(
                            color: AppColors.color373737,
                            fontSize: 20,
                          ),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                        child: Center(
                          child: Container(
                            margin:
                                EdgeInsetsDirectional.only(start: 1, end: 1),
                            height: 1,
                            width: MediaQuery.of(context).size.width - 40,
                            color: AppColors.colorC6C6C8,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        width: MediaQuery.of(context).size.width,
                        height: 45,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [..._buildListTypeTask()],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                        child: Center(
                          child: Container(
                            margin:
                                EdgeInsetsDirectional.only(start: 1, end: 1),
                            height: 1,
                            width: MediaQuery.of(context).size.width - 40,
                            color: AppColors.colorC6C6C8,
                          ),
                        ),
                      ),
                      Container(
                          child: Padding(
                        padding:
                            const EdgeInsets.only(left: 10, top: 7, right: 10),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'content',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: AppColors.colorB7B7B7,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: AppColors.colorF2F2F7),
                              child: TextFormField(
                                key: const ValueKey('name_edit_task_key'),
                                controller: _controllerContent,
                                style: TextStyle(
                                    fontSize: 17,
                                    color: AppColors.color94BEEF,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.left,
                                cursorColor: AppColors.colorEC407A,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 15, right: 15),
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  hintText: 'Please Input your content',
                                  hintStyle: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 17,
                                      color: AppColors.colorB7B7B7),
                                ),
                                onEditingComplete: () {
                                  FocusScope.of(context).nextFocus();
                                },
                              ),
                            ),
                          ],
                        ),
                      )),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 40,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Choose date',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: AppColors.color404040,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                InkWell(
                                  onTap: () async {
                                    _selectDate(context);
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        getDateFromTo(
                                            selectedDate ?? DateTime.now()),
                                        style: TextStyle(
                                            color: AppColors.color404040,
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 20),
                                        child: SvgPicture.asset(
                                          'assets/svg/down.svg',
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                              ],
                            )),
                            Container(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      'Choose time',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: AppColors.color404040,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    InkWell(
                                      onTap: () async {
                                        TimeRange result =
                                            await showTimeRangePicker(
                                          context: context,
                                          start: TimeOfDay(hour: 22, minute: 9),
                                          onStartChange: (start) {
                                            setState(() {
                                              timeStart = start.hour
                                                      .toString()
                                                      .padLeft(2, '0') +
                                                  ":" +
                                                  start.minute
                                                      .toString()
                                                      .padLeft(2, '0');
                                            });
                                          },
                                          onEndChange: (end) {
                                            setState(() {
                                              timeEnd = end.hour
                                                      .toString()
                                                      .padLeft(2, '0') +
                                                  ":" +
                                                  end.minute
                                                      .toString()
                                                      .padLeft(2, '0');
                                            });
                                          },
                                          interval: Duration(minutes: 30),
                                          use24HourFormat: true,
                                          padding: 30,
                                          strokeWidth: 20,
                                          handlerRadius: 14,
                                          strokeColor: Colors.blue,
                                          handlerColor: Colors.blue[700],
                                          selectedColor: Colors.blue[100],
                                          backgroundColor:
                                              Colors.black.withOpacity(0.3),
                                          ticks: 12,
                                          ticksColor: Colors.white,
                                          snap: true,
                                          labels: [
                                            "12 pm",
                                            "3 am",
                                            "6 am",
                                            "9 am",
                                            "12 am",
                                            "3 pm",
                                            "6 pm",
                                            "9 pm"
                                          ].asMap().entries.map((e) {
                                            return ClockLabel.fromIndex(
                                                idx: e.key,
                                                length: 8,
                                                text: e.value);
                                          }).toList(),
                                          labelOffset: 22,
                                          labelStyle: TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold),
                                          timeTextStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 24,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.bold),
                                          activeTimeTextStyle: TextStyle(
                                              color: Colors.white,
                                              fontSize: 26,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.bold),
                                        );
                                      },
                                      child: Row(
                                        children: [
                                          Text(
                                            timeStart + " - " + timeEnd,
                                            style: TextStyle(
                                                color: AppColors.color404040,
                                                fontSize: 13,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 20),
                                            child: SvgPicture.asset(
                                              'assets/svg/down.svg',
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                  ]),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Choose Status',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: AppColors.color404040,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              DropdownButton(
                                value: dropdownvalue,
                                icon: Icon(Icons.keyboard_arrow_down),
                                items: items.map((String items) {
                                  return DropdownMenuItem(
                                      value: items, child: Text(items));
                                }).toList(),
                                onChanged: (newValue) {
                                  if (newValue != null) {
                                    setState(() {
                                      dropdownvalue = newValue.toString();
                                    });
                                  }
                                },
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          )),
                      GestureDetector(
                        key: const ValueKey('edit_task_key'),
                        onTap: () {
                          editTask(
                              context,
                              getDateFromTo(selectedDate ?? DateTime.now()),
                              timeStart + " - " + timeEnd,
                              _controllerContent.text,
                              indexTask,
                              dropdownvalue);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                              colors: <Color>[
                                AppColors.color7EB6FF,
                                AppColors.color5F87E7
                              ],
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, 18, 0, 18),
                            child: Center(
                              child: Text(
                                AppString.titleEditTask,
                                style: TextStyle(
                                    color: AppColors.colorFCFCFC,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.2 - 25,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 53,
                  height: 53,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[
                          AppColors.colorF85BC4,
                          AppColors.colorE2189F
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: AppColors.colorE2189F.withOpacity(0.5),
                          spreadRadius: 0,
                          blurRadius: 5,
                          offset: Offset(0, 3),
                        )
                      ]),
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: SvgPicture.asset(
                      'assets/svg/close.svg',
                      color: AppColors.colorFCFCFC,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void _selectDate(BuildContext context) async {
    final ThemeData theme = Theme.of(context);
    switch (theme.platform) {
      case TargetPlatform.android:
        return buildMaterialDatePicker(context);
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return buildMaterialDatePicker(context);
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
        return buildCupertinoDatePicker(context);
    }
  }

  /// This builds material date picker in Android
  buildMaterialDatePicker(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate ?? DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  /// This builds cupertion date picker in iOS
  buildCupertinoDatePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != selectedDate)
                  setState(() {
                    selectedDate = picked;
                  });
              },
              initialDateTime: selectedDate,
              minimumYear: 2000,
              maximumYear: 2025,
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    _controllerContent.text = widget.task.content;
    indexTask = getIndexType(widget.task.type);
    timeEnd = widget.task.date.substring(19, 24);
    timeStart = widget.task.date.substring(11, 16);
    selectedDate = new DateFormat("dd-MM-yyyy hh:mm:ss")
        .parse(widget.task.date.substring(0, 10).trim() + " 00:00:00");
    widget.task.status
        ? dropdownvalue = "Completed"
        : dropdownvalue = "InCompleted";
  }

  int getIndexType(TypeTask type) {
    for (var i = 0; i < listTypeTask.length; i++) {
      if (listTypeTask[i].name == type.name) return i;
    }
    return 0;
  }

  String getDateFromTo(DateTime time) {
    return time.day.toString().padLeft(2, '0') +
        "-" +
        time.month.toString().padLeft(2, '0') +
        "-" +
        time.year.toString();
  }

  void getTimeFromTo() {
    selectedDate = DateTime.now();
    DateTime now = DateTime.now();
    timeStart = (now.hour - 1).toString().padLeft(2, '0') +
        ":" +
        now.minute.toString().padLeft(2, '0');
    timeEnd = now.hour.toString().padLeft(2, '0') +
        ":" +
        now.minute.toString().padLeft(2, '0');
  }

  void editTask(BuildContext context, String date, String time, String content,
      indexType, String status) {
    if (indexTask == -1) {
      ApplicationHelper.shared
          .showErrorDialog(context, "You must choose type of task", "Ok");
    } else {
      final bloc = context.read<EditTaskBloc>();

      bloc.add(EditTaskInitEvent(
        Task(widget.task.id, date + " " + time, content,
            listTypeTask[indexType], status == "InCompleted" ? false : true),
      ));
    }
  }

  List<Widget> _buildListTypeTask() {
    List<Widget> cards = [];
    for (var i = 0; i < listTypeTask.length; i++) {
      cards.add(InkWell(
        onTap: () {
          setState(() {
            indexTask = i;
          });
        },
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(
              color:
                  indexTask == i ? listTypeTask[i].color : Colors.transparent,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: [
                Visibility(
                  visible: indexTask != i,
                  child: Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: listTypeTask[i].color),
                  ),
                ),
                Visibility(
                  visible: indexTask != i,
                  child: SizedBox(
                    width: 5,
                  ),
                ),
                Text(
                  listTypeTask[i].name,
                  style: TextStyle(
                      fontSize: 15,
                      color: indexTask == i
                          ? AppColors.colorFFFFFF
                          : AppColors.color8E8E8E),
                ),
              ],
            ),
          ),
        ),
      ));
    }
    return cards;
  }
}
