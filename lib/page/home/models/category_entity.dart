import 'package:flutter/material.dart';

class CategoryTask {
  final String name;
  final Color color;
  final String icon;

  CategoryTask(this.name, this.color, this.icon);
}