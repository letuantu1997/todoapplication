import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todo_app/page/editTask/repository/edit_task_repository.dart';
import 'package:todo_app/page/home/models/task_entity.dart';

part 'edittask_event.dart';
part 'edittask_state.dart';

class EditTaskBloc extends Bloc<EditTaskEvent, EditTaskState> {
  EditTaskBloc() : super(EditTaskState(status: EditTaskStatus.Init));
  @override
  Stream<EditTaskState> mapEventToState(EditTaskEvent event) async* {
    if (event is EditTaskInitEvent) {
      yield state.copyWith(status: EditTaskStatus.Processing);
      yield* _editTask(event);
    } else if (event is EditTaskSuccessEvent) {
      yield state.copyWith(status: EditTaskStatus.Success);
    } else if (event is EditTaskFailedEvent) {
      yield state.copyWith(status: EditTaskStatus.Failed);
    }
  }

  Stream<EditTaskState> _editTask(EditTaskInitEvent event) async* {
    final repository = EditTaskRepository();
    try {
      repository.initDatabase((data) {
        if (data == true) {
          repository.editTask(event.task, (value) {
            if (value) {
              add(EditTaskSuccessEvent());
            } else {
              add(EditTaskFailedEvent());
            }
          });
        }
      });
    } on Exception catch (_) {
      yield state.copyWith(
        status: EditTaskStatus.Failed,
      );
    }
  }
}
