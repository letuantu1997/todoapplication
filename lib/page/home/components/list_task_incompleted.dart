import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/helpers/application_helper.dart';
import 'package:todo_app/page/editTask/widgets/edit_task.dart';
import 'package:todo_app/page/home/bloc/task_bloc.dart';
import 'package:todo_app/page/home/components/empty_task.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:todo_app/page/home/widgets/task_card.dart';

class ListTaskInCompleted extends StatefulWidget {
  @override
  _ListTaskInCompletedState createState() => _ListTaskInCompletedState();
}

List<Task> listTaskChoose = [];

class _ListTaskInCompletedState extends State<ListTaskInCompleted> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskBloc, TaskState>(
      listener: (context, state) {
        if (state.status == TaskStatus.UpdateSuccess) {
          final bloc = context.read<TaskBloc>();

          bloc.add(TaskInCompletedEvent());
        } else if (state.status == TaskStatus.DeleteSuccess) {
          final bloc = context.read<TaskBloc>();

          bloc.add(TaskInCompletedEvent());
        } else if (state.status == TaskStatus.UpdateFailed) {
          ApplicationHelper.shared
              .showErrorDialog(context, "Can't change status of Status", "Ok");
        } else if (state.status == TaskStatus.UpdateProcessing) {
        } else if (state.status == TaskStatus.SuccessInCompleted) {
          setState(() {
            listTaskChoose = [];
          });
        }
      },
      builder: (context, state) {
        return (state.listTaskInCompleted!.length) > 0
            ? Scaffold(
                body: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(18, 13, 18, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              AppString.nameApp,
                              style: TextStyle(
                                  color: AppColors.color8B87B3,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500),
                            ),
                            GestureDetector(
                              onTap: () {
                                final bloc = context.read<TaskBloc>();
                                bloc.add(
                                    UpdateListStatusTaskEvent(listTaskChoose));
                              },
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      AppColors.color5DE61A,
                                      AppColors.color39A801
                                    ],
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 18, 20, 18),
                                  child: Center(
                                    child: Text(
                                      AppString.done,
                                      style: TextStyle(
                                          color: AppColors.colorFCFCFC,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ..._buildListCardTask(state.listTaskInCompleted ?? []),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : EmptyTask();
      },
    );
  }

  List<Widget> _buildListCardTask(List<Task> list) {
    List<Widget> cards = [];
    for (var i = 0; i < list.length; i++) {
      cards.add(
        Slidable(
            key: Key(list[i].id.toString()),
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            secondaryActions: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    final bloc = context.read<TaskBloc>();

                    bloc.add(DeleteTaskEvent(list[i].id));
                  });
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  width: 35,
                  height: 35,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: AppColors.colorFFCFCF),
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      'assets/svg/trash.svg',
                    ),
                  ),
                ),
              ),
            ],
            child: GestureDetector(
              onTap: () async {
                await showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  backgroundColor: Colors.transparent,
                  builder: (BuildContext context) {
                    return EditTask(list[i]);
                  },
                );
                setState(() {
                  final bloc = context.read<TaskBloc>();

                  bloc.add(TaskInCompletedEvent());
                });
              },
              child: TaskCard(
                id: list[i].id,
                date: list[i].date,
                content: list[i].content,
                type: list[i].type,
                isNoti: list[i].status,
                onchange: (value) {
                  if (value) {
                    if (listTaskChoose.length == 0) {
                      list[i].status = true;
                      listTaskChoose.add(list[i]);
                    } else {
                      if (!listTaskChoose.contains(list[i])) {
                        list[i].status = true;
                        listTaskChoose.add(list[i]);
                      }
                    }
                  } else {
                    if (listTaskChoose.contains(list[i])) {
                      listTaskChoose.removeAt(i);
                    }
                  }
                },
              ),
            )),
      );
    }
    return cards;
  }
}
