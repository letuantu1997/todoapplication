import 'package:flutter/material.dart';
import 'package:todo_app/helpers/todo_helper.dart';
import 'package:todo_app/page/home/models/task_entity.dart';

class TaskRepository {
  void getTaskAll(
    ValueSetter<dynamic> completion,
  ) async {
    var result = await ToDoHelper.instance.getTaskAll();
    completion(result);
  }

  void getTask(
    int status,
    ValueSetter<dynamic> completion,
  ) async {
    var result = await ToDoHelper.instance.getTaskByStatus(status);
    completion(result);
  }

  void deleteTask(
    int id,
    ValueSetter<dynamic> completion,
  ) async {
    var result = await ToDoHelper.instance.deleteTask(id);
    result > 0 ? completion(true) : completion(false);
  }

  void updateTask(
    Task task,
    ValueSetter<dynamic> completion,
  ) async {
    var result = await ToDoHelper.instance.updateTask(task);
    result > 0 ? completion(true) : completion(false);
  }

  void updateListTask(
    List<Task> task,
    ValueSetter<dynamic> completion,
  ) async {
    List<bool> isCheck = [];
    task.forEach((Task element) async {
      var result = await ToDoHelper.instance.updateTask(element);
      result > 0 ? isCheck.add(true) : isCheck.add(false);
      if (isCheck.length == task.length) completion(true);
    });
  }
}
