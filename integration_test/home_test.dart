import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:todo_app/page/addTask/widgets/add_task.dart';
import 'package:todo_app/page/home/bloc/task_bloc.dart';
import 'package:todo_app/page/home/views/home_page.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  Widget homeWidget = MaterialApp(
    home: BlocProvider(
      create: (context) => TaskBloc(),
      child: HomeView(),
    ),
  );
  group('Home Screen test ', () {
    testWidgets('Home Screen Test When User click List type task',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pump();
      await tester.drag(find.byType(ListView), const Offset(0.0, -300));
      await tester.pump();
    });
    testWidgets('Home Screen Test When User click Tab Completed',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('tab_completed')));
    });
    testWidgets('Home Screen Test When User click Tab InCompleted',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('tab_incompleted')));
    });
    testWidgets('Home Screen Test When User click Tab all',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('tab_all')));
    });

    testWidgets('Home Screen Test When User click button adđ',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('button_add')));
      await tester.pumpAndSettle(Duration(seconds: 1));
      expect(find.byWidgetPredicate((widget) => widget is AddTask),
          findsOneWidget);
    });
    testWidgets('Home Screen Test When User add one task',
        (WidgetTester tester) async {
      await tester.pumpWidget(homeWidget);
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('button_add')));
      await tester.pumpAndSettle(Duration(seconds: 1));
      var txtName = find.byKey(
        const ValueKey('name_task_key'),
      );
      await tester.enterText(txtName, 'hi');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.tap(find.byKey(
        const ValueKey('add_task_key'),
      ));
    });
  });

  // group('Home Screen Test When User scroll  Tab All', () {
  //   testWidgets('Home Screen Test When User click Tab All ,click each item',
  //       (WidgetTester tester) async {
  //     await tester.pumpWidget(homeWidget);
  //     await tester.pumpAndSettle();
  //     await tester.tap(find.byKey(const ValueKey('tab_all')));
  //     await tester.pumpAndSettle(Duration(seconds: 1));
  //     await tester.tap(find.byKey(const Key('item_task')));
  //     await tester.pumpAndSettle(Duration(seconds: 1));
  //     var txtForm = find.byKey(
  //       const ValueKey('name_edit_task_key'),
  //     );
  //     await tester.enterText(txtForm, 'hi');
  //     await tester.testTextInput.receiveAction(TextInputAction.done);
  //     await tester.tap(find.byKey(
  //       const ValueKey('edit_task_key'),
  //     ));
  //   });
  // });
}
