import 'package:flutter/material.dart';
import 'package:todo_app/constants/colors.dart';

class TypeTask {
  final String name;
  final Color color;
  TypeTask(this.name, this.color);
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'color': color,
    };
  }

  factory TypeTask.fromJson(Map<String, dynamic> json) {
    return TypeTask(
      json["name"],
      json["color"],
    );
  }
}

Color colorTypeNameTask(String nameType) {
  for (var item in listTypeTask) {
    if (item.name == nameType) {
      return item.color;
    }
  }
  return AppColors.colorFFD506;
}

List<TypeTask> listTypeTask = [
  TypeTask('Order', AppColors.color09ACCE),
  TypeTask('Personal', AppColors.colorFFD506),
  TypeTask('Work', AppColors.color1ED102),
  TypeTask('Meeting', AppColors.colorD10263),
  TypeTask('Study', AppColors.color3044F2),
  TypeTask('Shopping', AppColors.colorF29130),
  TypeTask('Party', AppColors.colorF59BFF),
];
