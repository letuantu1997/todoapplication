import 'package:flutter/material.dart';
import 'package:todo_app/helpers/todo_helper.dart';
import 'package:todo_app/page/home/models/task_entity.dart';

class EditTaskRepository {
  void initDatabase(
    ValueSetter<dynamic> completion,
  ) {
    completion(true);
  }

  void editTask(
    Task task,
    ValueSetter<dynamic> completion,
  ) async {
    await ToDoHelper.instance
        .updateTask(task)
        .then((value) => value > 0 ? completion(true) : completion(false));
  }
}
