part of 'task_bloc.dart';

abstract class TaskEvent extends Equatable {
  const TaskEvent();

  @override
  List<Object> get props => [];
}

class TaskAllEvent extends TaskEvent {}

class TaskAllSuccessEvent extends TaskEvent {
  final List<Task> taskList;
  TaskAllSuccessEvent(this.taskList);
}

class TaskAllFailedEvent extends TaskEvent {}

class TaskCompletedEvent extends TaskEvent {}

class TaskCompletedSuccessEvent extends TaskEvent {
  final List<Task> taskList;
  TaskCompletedSuccessEvent(this.taskList);
}

class TaskCompletedFailedEvent extends TaskEvent {}

class TaskInCompletedEvent extends TaskEvent {}

class TaskInCompletedSuccessEvent extends TaskEvent {
  final List<Task> taskList;
  TaskInCompletedSuccessEvent(this.taskList);
}

class TaskInCompletedFailedEvent extends TaskEvent {}

class UpdateStatusTaskEvent extends TaskEvent {
  final Task task;
  UpdateStatusTaskEvent(this.task);
}

class UpdateStatusTaskSuccessEvent extends TaskEvent {}

class UpdateStatusTaskFailedEvent extends TaskEvent {}

class UpdateListStatusTaskEvent extends TaskEvent {
  final List<Task> task;
  UpdateListStatusTaskEvent(this.task);
}

class UpdateListStatusTaskSuccessEvent extends TaskEvent {}

class UpdateListStatusTaskFailedEvent extends TaskEvent {}

class DeleteTaskEvent extends TaskEvent {
  final int idTask;
  DeleteTaskEvent(this.idTask);
}

class DeleteTaskSuccessEvent extends TaskEvent {}

class DeleteTaskFailedEvent extends TaskEvent {}
