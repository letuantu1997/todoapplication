import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CheckBox extends StatefulWidget {
  final bool isChecked;
  final Function(bool) onchange;
  final double width;
  final double height;
  const CheckBox(
      {required this.isChecked,
      required this.onchange,
      required this.width,
      required this.height});
  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool isChecked = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      isChecked = widget.isChecked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
          widget.onchange(isChecked);
        });
      },
      child: Container(
        width: widget.width,
        height: widget.height,
        child: Center(
          child: isChecked
              ? SvgPicture.asset('assets/svg/ic_checked.svg')
              : SvgPicture.asset('assets/svg/ic_unchecked.svg'),
        ),
      ),
    );
  }
}
