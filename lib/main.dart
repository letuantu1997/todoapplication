import 'package:flutter/material.dart';
import 'package:todo_app/config/routers.dart';
import 'package:todo_app/helpers/session_helper.dart';
import 'package:todo_app/page/home/views/home_page.dart';
import 'package:todo_app/page/onboarding/views/onboard_page.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SessionHelper.shared.loadSession();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      initialRoute: intialRoute(),
      routes: {
        Routers.onBoarding: (context) => OnboardPage(),
        Routers.home: (context) => HomePage()
      },
    );
  }

  String intialRoute() {
    if (SessionHelper.shared.isStart) {
      return Routers.home;
    } else {
      return Routers.onBoarding;
    }
  }
}
