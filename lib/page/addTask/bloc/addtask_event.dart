part of 'addtask_bloc.dart';

abstract class AddTaskEvent extends Equatable {
  const AddTaskEvent();

  @override
  List<Object?> get props => [];
}

class AddTaskInitEvent extends AddTaskEvent {
  final Task task;
  AddTaskInitEvent(this.task);
}

class AddTaskSuccessEvent extends AddTaskEvent {}

class AddTaskFailedEvent extends AddTaskEvent {}
