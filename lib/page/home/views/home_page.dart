import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/page/addTask/widgets/add_task.dart';
import 'package:todo_app/page/home/bloc/task_bloc.dart';
import 'package:todo_app/page/home/components/header.dart';
import 'package:todo_app/page/home/components/list_all_task.dart';
import 'package:todo_app/page/home/components/list_task_completed.dart';
import 'package:todo_app/page/home/components/list_task_incompleted.dart';
import 'package:todo_app/page/home/models/task_entity.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: ((context) => TaskBloc()
          ..add(TaskAllEvent())
          ..add(TaskCompletedEvent())
          ..add(TaskInCompletedEvent())),
        child: HomeView());
  }
}

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

late int index;
List<Task> list = [];

void refreshPage(BuildContext context, int index) {
  switch (index) {
    case 0:
      final bloc = context.read<TaskBloc>();
      bloc.add(TaskAllEvent());
      break;
    case 1:
      final bloc = context.read<TaskBloc>();
      bloc.add(TaskCompletedEvent());
      break;
    case 2:
      final bloc = context.read<TaskBloc>();
      bloc.add(TaskInCompletedEvent());
      break;
    default:
  }
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
    index = 0;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskBloc, TaskState>(
      listener: (context, state) {},
      builder: (context, state) {
        List<Widget> pages = [
          ListAllTask(),
          ListTaskCompleted(),
          ListTaskInCompleted()
        ];
        return Scaffold(
          body: Container(
            child: Column(
              children: [
                Header(index),
                Expanded(child: pages[index]),
              ],
            ),
          ),
          //button Plus
          floatingActionButton: InkWell(
            key: const ValueKey('button_add'),
            onTap: () async {
              await showModalBottomSheet<void>(
                isScrollControlled: true,
                context: context,
                backgroundColor: Colors.transparent,
                builder: (BuildContext context) {
                  return AddTaskView();
                },
              );
              setState(() {
                refreshPage(context, index);
              });
            },
            child: Container(
              width: 53,
              height: 53,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[
                      AppColors.colorF85BC4,
                      AppColors.colorE2189F
                    ],
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.colorE2189F.withOpacity(0.5),
                      spreadRadius: 0,
                      blurRadius: 5,
                      offset: Offset(0, 4),
                    )
                  ]),
              child: Padding(
                padding: EdgeInsets.all(12),
                child: SvgPicture.asset(
                  'assets/svg/add.svg',
                  color: AppColors.colorFCFCFC,
                ),
              ),
            ),
          ),
          //button center navigator bar
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          bottomNavigationBar: BottomAppBar(
            color: AppColors.colorFFFFFF,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      key: ValueKey('tab_all'),
                      onTap: () {
                        setState(() {
                          index = 0;
                          final bloc = context.read<TaskBloc>();
                          bloc.add(TaskAllEvent());
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/svg/home.svg',
                            color: index == 0
                                ? AppColors.color5F87E7
                                : AppColors.color9F9F9F,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            AppString.all,
                            style: TextStyle(
                                color: index == 0
                                    ? AppColors.color5F87E7
                                    : AppColors.color9F9F9F,
                                fontSize: 10,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      key: ValueKey('tab_completed'),
                      onTap: () {
                        setState(() {
                          index = 1;
                          final bloc = context.read<TaskBloc>();
                          bloc.add(TaskCompletedEvent());
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/svg/task.svg',
                            color: index == 1
                                ? AppColors.color5F87E7
                                : AppColors.color9F9F9F,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            AppString.completed,
                            style: TextStyle(
                                color: index == 1
                                    ? AppColors.color5F87E7
                                    : AppColors.color9F9F9F,
                                fontSize: 10,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      key: ValueKey('tab_incompleted'),
                      onTap: () {
                        setState(() {
                          index = 2;
                          final bloc = context.read<TaskBloc>();
                          bloc.add(TaskInCompletedEvent());
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/svg/presentation.svg',
                            color: index == 2
                                ? AppColors.color5F87E7
                                : AppColors.color9F9F9F,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            AppString.incompleted,
                            style: TextStyle(
                                color: index == 2
                                    ? AppColors.color5F87E7
                                    : AppColors.color9F9F9F,
                                fontSize: 10,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
