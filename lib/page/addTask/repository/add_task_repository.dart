import 'package:todo_app/helpers/todo_helper.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:flutter/material.dart';

class AddTaskRepository {
  void initDatabase(
    ValueSetter<dynamic> completion,
  ) {
    completion(true);
  }

  void createTask(
    Task task,
    ValueSetter<dynamic> completion,
  ) async {
    await ToDoHelper.instance
        .insertTask(task)
        .then((value) => value > 0 ? completion(true) : completion(false));
  }
}
