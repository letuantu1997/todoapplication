part of 'task_bloc.dart';

class TaskState extends Equatable {
  final TaskStatus status;
  List<Task>? listTaskAll = [];
  List<Task>? listTaskCompleted = [];

  List<Task>? listTaskInCompleted = [];

  TaskState({required this.status});
  @override
  List<Object?> get props => [status];
  TaskState copyWith(
      {TaskStatus? status,
      List<Task>? listTaskAll,
      List<Task>? listTaskInCompleted,
      List<Task>? listTaskCompleted}) {
    var newState = TaskState(
      status: status ?? this.status,
    );
    if (listTaskInCompleted != null) {
      newState.listTaskInCompleted = listTaskInCompleted;
    } else {
      newState.listTaskInCompleted = this.listTaskInCompleted;
    }
    if (listTaskCompleted != null) {
      newState.listTaskCompleted = listTaskCompleted;
    } else {
      newState.listTaskCompleted = this.listTaskCompleted;
    }

    if (listTaskAll != null) {
      newState.listTaskAll = listTaskAll;
    } else {
      newState.listTaskAll = this.listTaskAll;
    }
    return newState;
  }
}

enum TaskStatus {
  Init,
  ProcessingAll,
  SuccessAll,
  FailedAll,
  ProcessingCompleted,
  SuccessCompleted,
  FailedCompleted,
  ProcessingInCompleted,
  SuccessInCompleted,
  FailedInCompleted,
  UpdateProcessing,
  UpdateSuccess,
  UpdateFailed,
  DeleteProcessing,
  DeleteSuccess,
  DeleteFailed,
}
