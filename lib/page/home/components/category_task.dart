import 'package:flutter/material.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/page/home/models/category_entity.dart';

class CategoryTaskItem extends StatefulWidget {
  const CategoryTaskItem({Key? key}) : super(key: key);

  @override
  _CategoryTaskItemState createState() => _CategoryTaskItemState();
}

List<CategoryTask> listTypeTask = [
  CategoryTask('Personal', AppColors.colorFFD506, 'assets/svg/user.svg'),
  CategoryTask('Work', AppColors.color1ED102, 'assets/svg/briefcase.svg'),
  CategoryTask('Meeting', AppColors.colorD10263, 'assets/svg/presentation.svg'),
  CategoryTask('Study', AppColors.colorF59BFF, 'assets/svg/molecule.svg'),
  CategoryTask(
      'Shopping', AppColors.colorF29130, 'assets/svg/shopping-basket.svg'),
  CategoryTask('Party', AppColors.color09ACCE, 'assets/svg/confetti.svg'),
];
int indexTask = -1;

class _CategoryTaskItemState extends State<CategoryTaskItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 45,
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: [..._buildListCardTask()],
      ),
    );
  }

  List<Widget> _buildListCardTask() {
    List<Widget> cards = [];
    for (var i = 0; i < listTypeTask.length; i++) {
      cards.add(InkWell(
        onTap: () {
          setState(() {
            indexTask = i;
          });
        },
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(
              color:
                  indexTask == i ? listTypeTask[i].color : Colors.transparent,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Row(
              children: [
                Visibility(
                  visible: indexTask != i,
                  child: Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: listTypeTask[i].color),
                  ),
                ),
                Visibility(
                  visible: indexTask != i,
                  child: SizedBox(
                    width: 5,
                  ),
                ),
                Text(
                  listTypeTask[i].name,
                  style: TextStyle(
                      fontSize: 15,
                      color: indexTask == i
                          ? AppColors.colorFFFFFF
                          : AppColors.color554E8F),
                ),
              ],
            ),
          ),
        ),
      ));
    }
    return cards;
  }
}
