import 'package:todo_app/session/session.dart';

class SessionHelper {
  static final SessionHelper shared = SessionHelper._internal();
  factory SessionHelper() {
    return shared;
  }
  bool isStart = false;

  SessionHelper._internal();

  Future<bool> loadSession() async {
    this.isStart = await ApplicationSession.loadSession();
    return isStart;
  }

  void insert(bool isStart) {
    ApplicationSession.insertSession(isStart);
  }

  void clearSession() {
    ApplicationSession.clearSesion();
  }
}
