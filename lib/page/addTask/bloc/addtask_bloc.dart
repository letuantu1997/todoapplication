import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todo_app/page/addTask/repository/add_task_repository.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
part 'addtask_event.dart';
part 'addtask_state.dart';

class AddTaskBloc extends Bloc<AddTaskEvent, AddTaskState> {
  AddTaskBloc() : super(AddTaskState(status: AddTaskStatus.Init));
  @override
  Stream<AddTaskState> mapEventToState(AddTaskEvent event) async* {
    if (event is AddTaskInitEvent) {
      yield state.copyWith(status: AddTaskStatus.Processing);
      yield* _addTask(event);
    } else if (event is AddTaskSuccessEvent) {
      yield state.copyWith(status: AddTaskStatus.Success);
    } else if (event is AddTaskFailedEvent) {
      yield state.copyWith(status: AddTaskStatus.Failed);
    }
  }

  Stream<AddTaskState> _addTask(AddTaskInitEvent event) async* {
    final repository = AddTaskRepository();
    try {
      repository.initDatabase((data) {
        if (data == true) {
          repository.createTask(event.task, (value) {
            if (value) {
              add(AddTaskSuccessEvent());
            } else {
              add(AddTaskFailedEvent());
            }
          });
        }
      });
    } on Exception catch (_) {
      yield state.copyWith(
        status: AddTaskStatus.Failed,
      );
    }
  }
}
