class AppString {
  static final String nameApp = 'ToDo';
  static final String welcome = 'Welcome';

  static final String titleOnboard = 'Reminders your task';
  static final String completed = 'Completed';
  static final String all = 'All';
  static final String incompleted = 'Incompleted';

  static final String contentOnboard =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pellentesque erat in blandit luctus.';
  static final String buttonGetStart = 'Get Started';
  static final String titleAddTask = 'Add task';
  static final String titleEditTask = 'Edit task';

  static final String done = 'Done';
  static final String reverse = 'Reverse';
}
