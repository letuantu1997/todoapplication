import 'package:flutter/material.dart';

class AppColors {
  static final Color color554E8F = HexColor('#554E8F');
  static final Color color82A0B7 = HexColor('#82A0B7');
  static final Color color5DE61A = HexColor('#5DE61A');
  static final Color color39A801 = HexColor('#39A801');
  static final Color colorFCFCFC = HexColor('#FCFCFC');
  static final Color color5F87E7 = HexColor('#5F87E7');
  static final Color color9F9F9F = HexColor('#9F9F9F');
  static final Color colorF85BC4 = HexColor('#F85BC4');
  static final Color colorE2189F = HexColor('#E2189F');
  static final Color color8B87B3 = HexColor('#8B87B3');
  static final Color colorC6C6C8 = HexColor('#C6C6C8');
  static final Color colorD9D9D9 = HexColor('#D9D9D9');
  static final Color colorFFDC00 = HexColor('#FFDC00');
  static final Color colorE8EBED = HexColor('#E8EBED');
  static final Color colorFFFFFF = HexColor('#FFFFFF');
  static final Color color404040 = HexColor('#404040');
  static final Color color373737 = HexColor('#373737');
  static final Color color3867D5 = HexColor('#3867D5');
  static final Color color81C7F5 = HexColor('#81C7F5');
  static final Color colorF1F1F1 = HexColor('#F1F1F1');
  static final Color colorF3F3F3 = HexColor('#F3F3F3');
  static final Color color94BEEF = HexColor('#94BEEF');
  static final Color colorFFD506 = HexColor('#FFD506');
  static final Color color1ED102 = HexColor('#1ED102');
  static final Color colorD10263 = HexColor('#D10263');
  static final Color color3044F2 = HexColor('#3044F2');
  static final Color colorF29130 = HexColor('#F29130');
  static final Color colorF59BFF = HexColor('#F59BFF');
  static final Color color8E8E8E = HexColor('#8E8E8E');
  static final Color color7EB6FF = HexColor('#7EB6FF');
  static final Color colorFFCFCF = HexColor('#FFCFCF');
  static final Color colorFB3636 = HexColor('#FB3636');
  static final Color color09ACCE = HexColor('#09ACCE');
  static final Color colorF2F2F7 = HexColor('#F2F2F7');
  static final Color colorEC407A = HexColor('#EC407A');
  static final Color colorB7B7B7 = HexColor('#B7B7B7');
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toLowerCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
