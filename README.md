# todo_app

A new Flutter project.

## Getting Started

Tool Visual Studio Code có cài sẵn môi trường flutter version: 1.0.0+1
environment:
sdk: ">=2.12.0 <3.0.0"

## How To run project :

Step 1 : gõ lệnh cmd flutter pub get để install dependencies
Step 2 : gõ lệnh cmd flutter run để run project
Step 3 : Nhấn button GetStart to bắt đầu
Step 4 : Tạo Task với các listType cho task để có thể load list
Step 5 : Update,Delete Status của task

## integration_test( folder integration_test/..)

gõ lệnh cmd flutter test/integration_test/{tenfile}.dart

# ex : flutter test integration_test/onboard_test.dart

## widget_test ( folder test/..)

gõ lệnh cmd flutter test/test/{tenfile}.dart

# ex : flutter test test/widget_onboard_test.dart

Nếu gặp Error: Cannot run with sound null safety, because the following dependencies
don't support null safety: - package:integration_test
=> thì nên thêm --no-sound-null-safety

# ex : flutter test --no-sound-null-safety integration_test/onboard_test.dart

Nếu terminmal hiển thị 00:07 +1: All tests passed! - > thì thành công
