import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/page/home/views/home_page.dart';
import 'package:todo_app/page/onboarding/views/onboard_page.dart';

void main() {
  //onBoard
  group('OnBoard Screen', () {
    testWidgets('OnBoard Screen Test When User click button',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: OnboardPage()),
      );
      await tester.tap(find.byKey(
        const ValueKey('button_start'),
      ));
      await tester.pumpAndSettle(Duration(seconds: 1));
      expect(find.byWidgetPredicate((widget) => widget is HomePage),
          findsOneWidget);
      await tester.pumpAndSettle(Duration(seconds: 1));
    });
  });
}
