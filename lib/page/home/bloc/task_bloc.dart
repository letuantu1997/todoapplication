import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:todo_app/page/home/repositpory/task_repository.dart';

part 'task_event.dart';
part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  TaskBloc() : super(TaskState(status: TaskStatus.Init));
  @override
  Stream<TaskState> mapEventToState(TaskEvent event) async* {
    if (event is TaskCompletedEvent) {
      yield state.copyWith(status: TaskStatus.ProcessingCompleted);
      yield* _getTaskComplete(event);
    } else if (event is TaskCompletedSuccessEvent) {
      yield state.copyWith(
          status: TaskStatus.SuccessCompleted,
          listTaskCompleted: event.taskList);
    } else if (event is TaskCompletedFailedEvent) {
      yield state.copyWith(status: TaskStatus.FailedCompleted);
    } else if (event is TaskInCompletedEvent) {
      yield state.copyWith(status: TaskStatus.ProcessingInCompleted);
      yield* _getTaskInComplete(event);
    } else if (event is TaskInCompletedSuccessEvent) {
      yield state.copyWith(
          status: TaskStatus.SuccessInCompleted,
          listTaskInCompleted: event.taskList);
    } else if (event is TaskInCompletedFailedEvent) {
      yield state.copyWith(status: TaskStatus.FailedInCompleted);
    } else if (event is UpdateStatusTaskEvent) {
      yield state.copyWith(status: TaskStatus.UpdateProcessing);
      yield* _updateTask(event);
    } else if (event is UpdateStatusTaskSuccessEvent) {
      yield state.copyWith(status: TaskStatus.UpdateSuccess);
    } else if (event is UpdateStatusTaskFailedEvent) {
      yield state.copyWith(status: TaskStatus.UpdateFailed);
    } else if (event is UpdateListStatusTaskEvent) {
      yield state.copyWith(status: TaskStatus.UpdateProcessing);
      yield* _updateListTask(event);
    } else if (event is UpdateListStatusTaskSuccessEvent) {
      yield state.copyWith(status: TaskStatus.UpdateSuccess);
    } else if (event is UpdateListStatusTaskFailedEvent) {
      yield state.copyWith(status: TaskStatus.UpdateFailed);
    } else if (event is DeleteTaskEvent) {
      yield state.copyWith(status: TaskStatus.DeleteProcessing);
      yield* _deleteTask(event);
    } else if (event is DeleteTaskSuccessEvent) {
      yield state.copyWith(status: TaskStatus.DeleteSuccess);
    } else if (event is DeleteTaskFailedEvent) {
      yield state.copyWith(status: TaskStatus.DeleteFailed);
    } else if (event is TaskAllEvent) {
      yield state.copyWith(status: TaskStatus.ProcessingAll);
      yield* _getTaskAll(event);
    } else if (event is TaskAllSuccessEvent) {
      yield state.copyWith(
          status: TaskStatus.SuccessAll, listTaskAll: event.taskList);
    } else if (event is TaskAllFailedEvent) {
      yield state.copyWith(status: TaskStatus.FailedAll);
    }
  }

  Stream<TaskState> _getTaskComplete(TaskCompletedEvent event) async* {
    final repository = TaskRepository();
    repository.getTask(1, (value) {
      if (value is List<Task>) {
        add(TaskCompletedSuccessEvent(value));
      } else {
        add(TaskInCompletedFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedCompleted,
      );
    }
  }

  Stream<TaskState> _getTaskAll(TaskAllEvent event) async* {
    final repository = TaskRepository();
    repository.getTaskAll((value) {
      if (value is List<Task>) {
        add(TaskAllSuccessEvent(value));
      } else {
        add(TaskAllFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedAll,
      );
    }
  }

  Stream<TaskState> _getTaskInComplete(TaskInCompletedEvent event) async* {
    final repository = TaskRepository();
    repository.getTask(0, (value) {
      if (value is List<Task>) {
        add(TaskInCompletedSuccessEvent(value));
      } else {
        add(TaskInCompletedFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedInCompleted,
      );
    }
  }

  Stream<TaskState> _deleteTask(DeleteTaskEvent event) async* {
    final repository = TaskRepository();
    repository.deleteTask(event.idTask, (value) {
      if (value) {
        add(DeleteTaskSuccessEvent());
      } else {
        add(DeleteTaskFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedInCompleted,
      );
    }
  }

  Stream<TaskState> _updateListTask(UpdateListStatusTaskEvent event) async* {
    final repository = TaskRepository();
    repository.updateListTask(event.task, (value) {
      if (value) {
        add(UpdateListStatusTaskSuccessEvent());
      } else {
        add(UpdateListStatusTaskFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedInCompleted,
      );
    }
  }

  Stream<TaskState> _updateTask(UpdateStatusTaskEvent event) async* {
    final repository = TaskRepository();
    repository.updateTask(event.task, (value) {
      if (value) {
        add(UpdateStatusTaskSuccessEvent());
      } else {
        add(UpdateStatusTaskFailedEvent());
      }
    });
    try {} on Exception catch (_) {
      yield state.copyWith(
        status: TaskStatus.FailedInCompleted,
      );
    }
  }
}
