import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/page/home/bloc/task_bloc.dart';
import 'package:todo_app/page/home/views/home_page.dart';

void main() {
  Widget homeWidget = MaterialApp(
    home: BlocProvider(
      create: (context) => TaskBloc(),
      child: HomeView(),
    ),
  );

  testWidgets('Home Screen Test Text', (WidgetTester tester) async {
    await tester.pumpWidget(homeWidget);
    await tester.pumpAndSettle();
    expect(find.text('Done'), findsOneWidget);
    await tester.pumpAndSettle(Duration(milliseconds: 2));

    expect(find.text('Reverse'), findsOneWidget);
    await tester.pumpAndSettle(Duration(milliseconds: 2));

    expect(find.text('ToDo'), findsOneWidget);
    await tester.pumpAndSettle(Duration(milliseconds: 2));

    expect(find.text('All'), findsNothing);
    await tester.pumpAndSettle(Duration(milliseconds: 2));

    expect(find.text('Completed'), findsNothing);
    await tester.pumpAndSettle(Duration(milliseconds: 2));

    expect(find.text('Incompleted'), findsNothing);
    await tester.pumpAndSettle(Duration(milliseconds: 2));
  });
}
