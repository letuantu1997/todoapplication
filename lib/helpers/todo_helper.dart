import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:todo_app/page/home/models/type_task_entity.dart';

class ToDoHelper {
  static final ToDoHelper instance = ToDoHelper._internal();
  factory ToDoHelper() {
    return instance;
  }
  static final _databaseName = "MyDatabase.db";
  static final _databaseVersion = 1;
  static final table = 'my_table';
  static final columnId = '_id';
  static final columnDate = 'date';
  static final columnContent = 'content';
  static final columnType = 'type';
  static final columnStatus = 'status';
  bool isStart = false;
  ToDoHelper._internal();
  late Database _database;
  Future<Database> get database async {
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onOpen: (db) {}, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(
        '''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY,
            $columnDate TEXT NOT NULL,
            $columnContent TEXT NOT NULL,
            $columnType TEXT NOT NULL,
            $columnStatus INTEGER NOT NULL
          )
          ''');
  }

  Future<List<Task>> getTaskAll() async {
    final db = await instance.database;
    List<Map<String, Object?>> results = await db.query(table);
    List<Task> tasks = [];
    results.forEach((result) {
      Task task = Task(
          result[columnId] as int,
          result[columnDate] as String,
          result[columnContent] as String,
          TypeTask(result[columnType] as String,
              colorTypeNameTask(result[columnType] as String)),
          (result[columnStatus] as int) == 1 ? true : false);
      tasks.add(task);
    });
    return tasks;
  }

  Future<List<Task>> getTaskByStatus(int status) async {
    final db = await instance.database;
    List<Map<String, Object?>> results =
        await db.query(table, where: "$columnStatus = ?", whereArgs: [status]);
    List<Task> tasks = [];
    results.forEach((result) {
      Task task = Task(
          result[columnId] as int,
          result[columnDate] as String,
          result[columnContent] as String,
          TypeTask(result[columnType] as String,
              colorTypeNameTask(result[columnType] as String)),
          (result[columnStatus] as int) == 1 ? true : false);
      tasks.add(task);
    });
    return tasks;
  }

  Future<int> insertTask(Task task) async {
    final db = await instance.database;
    Map<String, dynamic> row = {
      columnDate: task.date,
      columnContent: task.content,
      columnType: task.type.name,
      columnStatus: task.status == true ? 1 : 0,
    };
    var result = await db.insert(table, row);
    return result;
  }

  Future<int> updateTask(Task task) async {
    final db = await database;
    Map<String, dynamic> row = {
      columnId: task.id,
      columnDate: task.date,
      columnContent: task.content,
      columnType: task.type.name,
      columnStatus: task.status == true ? 1 : 0,
    };
    var result = await db
        .update(table, row, where: "$columnId = ?", whereArgs: [task.id]);
    return result;
  }

  Future<int> deleteTask(int id) async {
    final db = await database;
    var result =
        await db.delete(table, where: "$columnId = ?", whereArgs: [id]);
    return result;
  }

  deleteDB() async {
    final db = await database;
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
    await db.close();
  }
}
