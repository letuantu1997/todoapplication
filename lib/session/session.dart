import 'package:shared_preferences/shared_preferences.dart';

class ApplicationSession {
  static final String key = "START_KEY";
  static Future<bool> loadSession() async {
    final prefs = await SharedPreferences.getInstance();
    final isStart = prefs.getBool(key);
    if (isStart != null) {
      return true;
    }
    return false;
  }

  static void clearSesion() {
    SharedPreferences.getInstance().then((prefs) => {prefs.remove(key)});
  }

  static void insertSession(bool isStart) {
    SharedPreferences.getInstance().then(
      (prefs) => prefs.setBool(key, isStart),
    );
  }
}
