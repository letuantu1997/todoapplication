import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/page/home/models/type_task_entity.dart';
import 'package:todo_app/page/home/widgets/checkbox.dart';

class TaskCard extends StatelessWidget {
  final int id;
  final String date;
  final String content;
  final TypeTask type;
  final bool isNoti;
  bool? disableCheck;

  Function(bool)? onchange;
  TaskCard(
      {required this.id,
      required this.date,
      required this.content,
      required this.type,
      required this.isNoti,
      this.onchange,
      this.disableCheck});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
      decoration: BoxDecoration(
          color: AppColors.colorFFFFFF,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: AppColors.colorE8EBED,
              spreadRadius: 0,
              blurRadius: 5,
              offset: Offset(0, 2),
            )
          ]),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: this.type.color,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    bottomLeft: Radius.circular(5),
                  )),
              width: 4,
              // height: 60,
            ),
            SizedBox(
              width: 18,
              height: 18,
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: this.disableCheck == null
                    ? CheckBox(
                        isChecked: false,
                        onchange: (isChecked) {
                          this.onchange?.call(isChecked);
                        },
                        width: 18,
                        height: 18)
                    : Container(
                        width: 18,
                        height: 18,
                        child: this.isNoti
                            ? Text(
                                'D',
                                style: TextStyle(
                                  color: AppColors.color5DE61A,
                                ),
                              )
                            : Text(
                                'In',
                                style: TextStyle(
                                  color: Colors.red,
                                ),
                              ))),
            SizedBox(
              width: 15,
            ),
            Container(
              width: 100,
              child: Text(
                this.date,
                style: TextStyle(
                    color: AppColors.colorC6C6C8,
                    fontSize: 13,
                    fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              flex: 2,
              child: Text(
                this.content,
                style: TextStyle(
                    color: AppColors.color554E8F,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            SvgPicture.asset(
              'assets/svg/bell.svg',
              color:
                  this.isNoti ? AppColors.colorFFDC00 : AppColors.colorD9D9D9,
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
