import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/helpers/application_helper.dart';
import 'package:todo_app/page/editTask/widgets/edit_task.dart';
import 'package:todo_app/page/home/bloc/task_bloc.dart';
import 'package:todo_app/page/home/components/empty_task.dart';
import 'package:todo_app/page/home/models/task_entity.dart';
import 'package:todo_app/page/home/widgets/task_card.dart';

class ListAllTask extends StatefulWidget {
  @override
  _ListAllTaskState createState() => _ListAllTaskState();
}

class _ListAllTaskState extends State<ListAllTask> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskBloc, TaskState>(
      listener: (context, state) {
        if (state.status == TaskStatus.SuccessAll) {
          ApplicationHelper.shared.hideDialogProgress(context);
        } else if (state.status == TaskStatus.FailedAll) {
          ApplicationHelper.shared.hideDialogProgress(context);
        } else if (state.status == TaskStatus.ProcessingAll) {
          ApplicationHelper.shared.showDialogProgress(context);
        } else if (state.status == TaskStatus.DeleteSuccess) {
          ApplicationHelper.shared.hideDialogProgress(context);
          final bloc = context.read<TaskBloc>();
          bloc.add(TaskAllEvent());
        } else if (state.status == TaskStatus.UpdateSuccess) {
          ApplicationHelper.shared.hideDialogProgress(context);
          final bloc = context.read<TaskBloc>();
          bloc.add(TaskAllEvent());
        }
      },
      builder: (context, state) {
        return (state.listTaskAll!.length) > 0
            ? Scaffold(
                body: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(18, 13, 18, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              AppString.nameApp,
                              style: TextStyle(
                                  color: AppColors.color8B87B3,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ..._buildListCardTask(state.listTaskAll ?? [], context),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : EmptyTask();
      },
    );
  }

  List<Widget> _buildListCardTask(List<Task> list, BuildContext context) {
    List<Widget> cards = [];
    for (var i = 0; i < list.length; i++) {
      cards.add(
        Slidable(
            key: Key(list[i].id.toString()),
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            secondaryActions: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    final bloc = context.read<TaskBloc>();

                    bloc.add(DeleteTaskEvent(list[i].id));
                  });
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  width: 35,
                  height: 35,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: AppColors.colorFFCFCF),
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      'assets/svg/trash.svg',
                    ),
                  ),
                ),
              ),
            ],
            child: GestureDetector(
              onTap: () async {
                await showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  backgroundColor: Colors.transparent,
                  builder: (BuildContext context) {
                    return EditTask(list[i]);
                  },
                );
                setState(() {
                  final bloc = context.read<TaskBloc>();

                  bloc.add(TaskAllEvent());
                });
              },
              child: TaskCard(
                id: list[i].id,
                date: list[i].date,
                content: list[i].content,
                type: list[i].type,
                isNoti: list[i].status,
                disableCheck: false,
              ),
            )),
      );
    }
    return cards;
  }
}
