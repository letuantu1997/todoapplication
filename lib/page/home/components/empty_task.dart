import 'package:flutter/material.dart';
import 'package:todo_app/constants/colors.dart';

class EmptyTask extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          child: Image.asset('assets/img/empty_note.png'),
        ),
        SizedBox(
          height: 60,
        ),
        Text(
          'No tasks',
          style: TextStyle(
              color: AppColors.color554E8F,
              fontSize: 22,
              fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'You have no task to do.',
          style: TextStyle(
              color: AppColors.color82A0B7,
              fontSize: 17,
              fontWeight: FontWeight.w400),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
