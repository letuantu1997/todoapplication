part of 'addtask_bloc.dart';

class AddTaskState extends Equatable {
  final AddTaskStatus status;
  AddTaskState({required this.status});
  @override
  List<Object?> get props => [status];
  AddTaskState copyWith({AddTaskStatus? status, Task? task}) {
    var newState = AddTaskState(
      status: status ?? this.status,
    );

    return newState;
  }
}

enum AddTaskStatus {
  Init,
  Processing,
  Success,
  Failed,
}
