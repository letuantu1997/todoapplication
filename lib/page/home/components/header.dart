import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/page/home/components/category_task.dart';

class Header extends StatelessWidget {
  final int indexTab;
  const Header(this.indexTab);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: <Color>[AppColors.color3867D5, AppColors.color81C7F5],
        ),
      ),
      child: Stack(
        children: [
          SvgPicture.asset(
            'assets/svg/bg.svg',
            color: AppColors.colorFFFFFF.withOpacity(0.1),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(18, 50, 18, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      this.indexTab == 0
                          ? AppString.completed
                          : AppString.incompleted,
                      style: TextStyle(
                          color: AppColors.colorF1F1F1,
                          fontSize: 17,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: AppColors.colorFFFFFF,
                    shape: BoxShape.circle,
                  ),
                  child: ClipOval(
                    child: Image.asset(
                      'assets/img/user.png',
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(18, 110, 18, 15),
            decoration: BoxDecoration(
                color: AppColors.color94BEEF.withOpacity(0.8),
                borderRadius: BorderRadius.circular(5)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [CategoryTaskItem()]),
          )
        ],
      ),
    );
  }
}
