part of 'edittask_bloc.dart';

abstract class EditTaskEvent extends Equatable {
  const EditTaskEvent();
  @override
  List<Object> get props => [];
}

class EditTaskInitEvent extends EditTaskEvent {
  final Task task;
  EditTaskInitEvent(this.task);
}

class EditTaskSuccessEvent extends EditTaskEvent {}

class EditTaskFailedEvent extends EditTaskEvent {}
