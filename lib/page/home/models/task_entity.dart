import 'package:todo_app/page/home/models/type_task_entity.dart';

class Task {
  int id;
  String date;
  String content;
  TypeTask type;
  bool status;
  Task(this.id, this.date, this.content, this.type, this.status);

  Map<String, Object?> toMap() {
    var map = <String, Object?>{
      "id": this.id,
      "date": this.date,
      "type": this.type.name,
      "content": this.content,
      "status": this.status == true ? 1 : 0
    };
    return map;
  }
}

List<Task> listTask = [
  Task(1, '02/11/2021 8:00', 'Metting 1', listTypeTask[0], false),
  Task(2, '02/11/2021 9:00', 'Metting 2', listTypeTask[1], false),
  Task(3, '02/11/2021 10:00', 'Metting 3', listTypeTask[2], false),
  Task(4, '02/11/2021 11:00', 'Metting 4', listTypeTask[3], false),
];
