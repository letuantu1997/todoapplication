part of 'edittask_bloc.dart';

class EditTaskState extends Equatable {
  final EditTaskStatus status;
  EditTaskState({required this.status});
  @override
  List<Object?> get props => [status];
  EditTaskState copyWith({EditTaskStatus? status}) {
    var newState = EditTaskState(
      status: status ?? this.status,
    );
    return newState;
  }
}

enum EditTaskStatus {
  Init,
  Processing,
  Success,
  Failed,
}
