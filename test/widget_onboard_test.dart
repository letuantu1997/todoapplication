import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/constants/string.dart';
import 'package:todo_app/page/onboarding/views/onboard_page.dart';

void main() {
  group('OnBoard Screen Test', () {
    // OnBoard
    testWidgets('OnBoard Screen Widget', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: OnboardPage()),
      );
      expect(find.text(AppString.titleOnboard), findsOneWidget);
      await tester.pumpAndSettle(const Duration(seconds: 1));

      expect(find.text(AppString.contentOnboard), findsOneWidget);
      await tester.pumpAndSettle(const Duration(seconds: 1));

      expect(find.text(AppString.buttonGetStart), findsOneWidget);

      await tester.pumpAndSettle(Duration(seconds: 1));
    });
  });
}
